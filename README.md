# hello-kubernetes

A simple node.js app running with Kubernetes.

Configure deployment and service
```bash
kubectl apply -f k8s/hello-express-namespace.yml
kubectl apply -f k8s
```

Make service avaiable in browser
```bash
minkube service hello-kubernetes
```

Create port forwarding for our node app:
```bash
kubectl port-forward service/hello-express-service 8000:1337
```

When we change our config, update the deployment:
```bash
kubectl rollout restart deployment.apps/hello-express-deployment
```

Note:
In order for the ingress to work there needs to be an ingress controller.
To create one in minikube, run

```bash
minikube addons enable ingress
```

## Todo
- [ ] Why is the container name "-container" not used?
- [ ] No rollout on every resource-changes? Get more familiar with rollouts.
- [ ] What exactly does the apiDefinition in a given resource spec?
- [ ] When to use static vs dynamic persistent-volume provisioning?

