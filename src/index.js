const express = require('express')
const app = express()

const mariadb = require('mariadb')
const pool = mariadb.createPool({
  host: process.env.DB_HOST || 'hello-express-db-container',
  database: process.env.DB_NAME || 'hello-express-db',
  user: process.env.DB_USER || 'user',
  password: process.env.DB_PASS || 'password',
  connectionLimit: 5
})

app.get('/', function (req, res) {
  res.json({
    status: 'ok',
    message: 'hello kubernetes!'
  })
})

app.get('/db', async function (req, res) {
  let conn
  try {
    conn = await pool.getConnection()
    const rows = await conn.query("SELECT * FROM foo")
    res.send(rows)
  } catch (err) {
    throw err
  } finally {
    if (conn) return conn.end()
  }
})

app.listen(3000, err => {
  if (err) {
    throw err
  }

  console.log('server is listening')
})
